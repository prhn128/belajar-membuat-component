import logo from './logo.svg';
import './App.css';
import React, { Component } from 'react';



function App(){
    return (
   <div className="App">
    <div className="nav">
      </div>
     <header className="App-header">
     <img src={logo} className="App-logo" alt="logo" />
      <h1>Farhan Futondony</h1>
      <h2>Absen 8</h2>
      </header>
    </div>
  );
}

const AppVar = () =>{
  const name ='Farhan Futondony'
  const grade = 'XI-RPL'
  return (
  <div className="App">
    <div className="nav">
      </div>
     <header className="App-header">
     <img src={logo} className="App-logo" alt="logo" />
      <h1>{name}</h1>
      <h2>{grade}</h2>
      </header>
    </div>
  );
}

class AppClass extends React.Component {
  constructor(props){
  super(props);
  this.name = 'Farhan Futondony';
  this.School = 'SMKN 10 Jakarta'
}
  render(){
    return(
    <div className="App">
    <div className="nav">
      </div>
     <header className="App-header">
     <img src={logo} className="App-logo" alt="logo" />
      <h1>{this.name}</h1>
      <h2>{this.School}</h2>
      </header>
    </div>
    );
  }
}

export {
  App,
  AppVar,
  AppClass
}
